package pt.rumos.rest_api.users;

import java.util.List;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController {
    
    @Autowired
    private UsersService service;
    
    @GetMapping("/users")
    public String getAllUsers(Model m){
        List<Users> entities = service.findAll();
        return new Gson().toJson(entities);
    }

}

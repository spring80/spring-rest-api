# REST API
> 2021-12-17 Carol Pereira

# deploy

activate profile  
`$ set spring_profiles_active=<profile>`

local run
`$ mvnw.cmd spring-boot:run`

local run with profile
`$ mvnw.cmd spring-boot:run -Dspring-boot.run.profiles=development`

run final solution
`$ java -jar target/rest_api-<version>.jar --spring.profiles.active=development`

---

## build
clean target
`$ mvnw.cmd clean`

target build
`$ mvnw.cmd package`

Docker build
```sh
$ git clone https://gitlab.com/spring80/spring-rest-api.git
cd spring-rest-api
$ docker build -t spring-rest-api:latest 
```

## CI/CD
`$ sudo apk add gitlab-runner`
```
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "mS-cZa_ryxWn53-twSeE" \
  --executor "shell" \
  --description "docker-lab-node1" \
  --tag-list "production_server_tag" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
  ```